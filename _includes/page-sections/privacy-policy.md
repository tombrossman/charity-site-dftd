### What information we collect about you
We collect the following types of information about you:
 - name
 - email address
 - address
 - telephone number
 - emergency contact name
 - emergency contact relationship to you
 - emergency contact telephone number
 - driving licence details
 - special category data concerning your health

### Why we collect information about you
We need to collect and hold information about you, in order to:
 - answer your query
 - provide you with information you need
 - provide the service you have requested

### How we use your information
We use the information held about you in the following ways:
 - To contact you regarding use of our services
 - To inform you of the outcome of your driving assessment
 - To keep track of your progress throughout a course of lessons
 - To inform, where appropriate, other organisations such as the relevant licencing authority, your GP, the Police, of the outcome of your driving assessment

 We will never sell your information or pass it on to a third party for marketing purposes or  for any other purpose unconnected with the services we provide.

 Sometimes it is necessary to share your information with other parties relevant to the  service such as:
 - Licence Issuer (Relevant Parish)
 - Licensing Authority (Driver and Vehicle Standards)
 - Health care professionals
 - Social and Welfare organisations
 - Family representative of the person whose data is being processed

We may also disclose your personal data to third parties
 - If we are under legal obligation to do so
 - To enforce or apply terms and conditions, or to protect the rights, property and safety of DriveAbility Jersey, their clients and team members

### Where do we store your data?

All information is stored in secure filing systems or on secure servers.

Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our website; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.

Our website may, from time to time, contain links to and from other websites including the websites of our clients or partners who have provided feedback or testimonials or networking or social media websites such as LinkedIn and Twitter. If you follow a link to any of these websites, please note that these websites have their own privacy policies and terms of use and we do not accept any responsibility or liability for these policies and terms of use. Please check these policies before you submit any personal data to these websites.

### Your rights
Under Data Protection legislation, you have various rights in connection with any personal data.

You have the right to ask us not to process your personal data for marketing purposes. We will inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at: driveabilityjersey@gmail.com

If you believe that any information we are holding on you is incorrect or incomplete, please contact us in writing as soon as possible using the address below. We will promptly correct any information found to be incorrect.

If you would like us to remove any records or personal information we hold on file, please contact us in writing and we will remove the requested record and information unless we specifically need to retain it to comply with a legal obligation.
